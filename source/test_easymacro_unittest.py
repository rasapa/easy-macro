#!/usr/bin/env python3
# coding: utf-8

import unittest
from libo import LIBO


class TestTools(unittest.TestCase):

    def setUp(self):
        self.app = LIBO()

    def tearDown(self):
        del self.app

    def test_name(self):
        expected = 'LibreOffice'
        result = self.app.NAME
        self.assertEqual(result, expected)

    def test_version(self):
        expected = '6.0'
        result = self.app.VERSION
        self.assertEqual(result, expected)

    def test_language(self):
        expected = 'es'
        result = self.app.LANGUAGE
        self.assertEqual(result, expected)

    def test_debug(self):
        expected = None
        result = self.app.debug('debug')
        self.assertEqual(result, expected)

    def test_get_info_pc(self):
        expected = 'mau'
        result = self.app.get_info_pc()[0]
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main()

