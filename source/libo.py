#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# == Rapid Develop Macros in LibreOffice ==

import csv
import datetime
import getpass
import json
import logging
import platform
import os
import re
import shutil
import subprocess
import sys
import threading
import time
from functools import wraps
from pprint import pprint

import uno
import unohelper
from com.sun.star.beans import PropertyValue, NamedValue
from com.sun.star.table.CellContentType import VALUE, TEXT, FORMULA
from com.sun.star.text.ControlCharacter import PARAGRAPH_BREAK
from com.sun.star.util import Time, Date, DateTime
from com.sun.star.awt import MessageBoxButtons as MSG_BUTTONS
from com.sun.star.awt.MessageBoxResults import YES
from com.sun.star.awt.PosSize import POSSIZE, SIZE
from com.sun.star.awt import Rectangle

from com.sun.star.lang import XEventListener
from com.sun.star.awt import XActionListener
from com.sun.star.awt import XMouseListener
from com.sun.star.awt import XTabListener
from com.sun.star.awt import XTopWindowListener
from com.sun.star.awt import XWindowListener
from com.sun.star.awt import XFocusListener
from com.sun.star.awt import XItemListener
from com.sun.star.awt import XMenuListener
from com.sun.star.awt import XKeyListener
from com.sun.star.awt import KeyEvent
from com.sun.star.view import XSelectionChangeListener


PY2 = sys.version_info.major == 2
FILE_NAME_DEBUG = 'debug.log'
WIN = 'win32'

frm = '%(asctime)s %(name)s %(levelname)s: %(message)s'
formatter = logging.Formatter(frm)
handler = logging.StreamHandler()
handler.setFormatter(formatter)
log = logging.getLogger(__name__)
log.addHandler(handler)
log.setLevel(logging.DEBUG)


CURRENT_PATH = os.path.split(__file__)[0]
CTX = uno.getComponentContext()
SM = CTX.getServiceManager()


_SEPARATION_X = 2
_SEPARATION_Y = 5
_SEPARATION = 10


def _create_instance(name, with_context=False):
    if with_context:
        instance = SM.createInstanceWithContext(name, CTX)
    else:
        instance = SM.createInstance(name)
    return instance


def _get_config(key, node_name):
    name = 'com.sun.star.configuration.ConfigurationProvider'
    service = 'com.sun.star.configuration.ConfigurationAccess'
    cp = _create_instance(name, True)
    node = PropertyValue()
    node.Name = 'nodepath'
    node.Value = node_name
    try:
        ca = cp.createInstanceWithArguments(service, (node,))
        if ca and (ca.hasByName(key)):
            data = ca.getPropertyValue(key)
        return data
    except Exception as e:
        log.error(e)
        return ''


LANGUAGE = _get_config('ooLocale', 'org.openoffice.Setup/L10N/')
NAME = TITLE = _get_config('ooName', 'org.openoffice.Setup/Product')
VERSION = _get_config('ooSetupVersion', 'org.openoffice.Setup/Product')


# ~ https://en.wikipedia.org/wiki/Web_colors
def _color(value):
    COLORS = {
        'aliceblue': 15792383,
        'antiquewhite': 16444375,
        'aqua': 65535,
        'aquamarine': 8388564,
        'azure': 15794175,
        'beige': 16119260,
        'bisque': 16770244,
        'black': 0,
        'blanchedalmond': 16772045,
        'blue': 255,
        'blueviolet': 9055202,
        'brown': 10824234,
        'burlywood': 14596231,
        'cadetblue': 6266528,
        'chartreuse': 8388352,
        'chocolate': 13789470,
        'coral': 16744272,
        'cornflowerblue': 6591981,
        'cornsilk': 16775388,
        'crimson': 14423100,
        'cyan': 65535,
        'darkblue': 139,
        'darkcyan': 35723,
        'darkgoldenrod': 12092939,
        'darkgray': 11119017,
        'darkgreen': 25600,
        'darkgrey': 11119017,
        'darkkhaki': 12433259,
        'darkmagenta': 9109643,
        'darkolivegreen': 5597999,
        'darkorange': 16747520,
        'darkorchid': 10040012,
        'darkred': 9109504,
        'darksalmon': 15308410,
        'darkseagreen': 9419919,
        'darkslateblue': 4734347,
        'darkslategray': 3100495,
        'darkslategrey': 3100495,
        'darkturquoise': 52945,
        'darkviolet': 9699539,
        'deeppink': 16716947,
        'deepskyblue': 49151,
        'dimgray': 6908265,
        'dimgrey': 6908265,
        'dodgerblue': 2003199,
        'firebrick': 11674146,
        'floralwhite': 16775920,
        'forestgreen': 2263842,
        'fuchsia': 16711935,
        'gainsboro': 14474460,
        'ghostwhite': 16316671,
        'gold': 16766720,
        'goldenrod': 14329120,
        'gray': 8421504,
        'grey': 8421504,
        'green': 32768,
        'greenyellow': 11403055,
        'honeydew': 15794160,
        'hotpink': 16738740,
        'indianred': 13458524,
        'indigo': 4915330,
        'ivory': 16777200,
        'khaki': 15787660,
        'lavender': 15132410,
        'lavenderblush': 16773365,
        'lawngreen': 8190976,
        'lemonchiffon': 16775885,
        'lightblue': 11393254,
        'lightcoral': 15761536,
        'lightcyan': 14745599,
        'lightgoldenrodyellow': 16448210,
        'lightgray': 13882323,
        'lightgreen': 9498256,
        'lightgrey': 13882323,
        'lightpink': 16758465,
        'lightsalmon': 16752762,
        'lightseagreen': 2142890,
        'lightskyblue': 8900346,
        'lightslategray': 7833753,
        'lightslategrey': 7833753,
        'lightsteelblue': 11584734,
        'lightyellow': 16777184,
        'lime': 65280,
        'limegreen': 3329330,
        'linen': 16445670,
        'magenta': 16711935,
        'maroon': 8388608,
        'mediumaquamarine': 6737322,
        'mediumblue': 205,
        'mediumorchid': 12211667,
        'mediumpurple': 9662683,
        'mediumseagreen': 3978097,
        'mediumslateblue': 8087790,
        'mediumspringgreen': 64154,
        'mediumturquoise': 4772300,
        'mediumvioletred': 13047173,
        'midnightblue': 1644912,
        'mintcream': 16121850,
        'mistyrose': 16770273,
        'moccasin': 16770229,
        'navajowhite': 16768685,
        'navy': 128,
        'oldlace': 16643558,
        'olive': 8421376,
        'olivedrab': 7048739,
        'orange': 16753920,
        'orangered': 16729344,
        'orchid': 14315734,
        'palegoldenrod': 15657130,
        'palegreen': 10025880,
        'paleturquoise': 11529966,
        'palevioletred': 14381203,
        'papayawhip': 16773077,
        'peachpuff': 16767673,
        'peru': 13468991,
        'pink': 16761035,
        'plum': 14524637,
        'powderblue': 11591910,
        'purple': 8388736,
        'red': 16711680,
        'rosybrown': 12357519,
        'royalblue': 4286945,
        'saddlebrown': 9127187,
        'salmon': 16416882,
        'sandybrown': 16032864,
        'seagreen': 3050327,
        'seashell': 16774638,
        'sienna': 10506797,
        'silver': 12632256,
        'skyblue': 8900331,
        'slateblue': 6970061,
        'slategray': 7372944,
        'slategrey': 7372944,
        'snow': 16775930,
        'springgreen': 65407,
        'steelblue': 4620980,
        'tan': 13808780,
        'teal': 32896,
        'thistle': 14204888,
        'tomato': 16737095,
        'turquoise': 4251856,
        'violet': 15631086,
        'wheat': 16113331,
        'white': 16777215,
        'whitesmoke': 16119285,
        'yellow': 16776960,
        'yellowgreen': 10145074,
    }

    if isinstance(value, tuple):
        return (value[0] << 16) + (value[1] << 8) + value[2]

    if isinstance(value, str) and value[0] == '#':
        r, g, b = bytes.fromhex(value[1:])
        return (r << 16) + (g << 8) + b

    if isinstance(value, int):
        return value

    return COLORS.get(value.lower(), -1)


COLORS = {
    'DEFAULT': 16777215,
    'INFOCUS': _color('yellow'),
    'BLUE': 255,
    'GRAY': 16119285,
    'GREEN': 12255176,
    'RED': 16764108,
}


def _(msg):
    L = LANGUAGE.split('-')[0]
    if L == 'en':
        return msg

    data = {
        'es': {
            'OK': 'Aceptar',
            'Cancel': 'Cancelar',
        }
    }

    if not L in data:
        return msg

    return data[L][msg]


def mri(obj):
    m = _create_instance('mytools.Mri')
    if not m is None:
        m.inspect(obj)
    return


def catch_exception(f):
    @wraps(f)
    def func(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except Exception as e:
            log.error(f.__name__, exc_info=True)
    return func


def run_in_thread(fn):
    def run(*k, **kw):
        t = threading.Thread(target=fn, args=k, kwargs=kw)
        t.start()
        return t
    return run


def _join(*paths):
    return os.path.join(*paths)


def _index(data, value, reverse=False):
    if reverse:
        for i in range(len(data)-1, -1, -1):
            if data[i] == value:
                return i
        return -1
    return data.index(value)


def _file_url(path):
    if path.startswith('file://'):
        return path
    return uno.systemPathToFileUrl(path)


def _system_path(path):
    if path.startswith('file://'):
        return os.path.abspath(uno.fileUrlToSystemPath(path))
    return path


def _get_path_setting(name='Work'):
    """
        Return de path name in config
        http://api.libreoffice.org/docs/idl/ref/interfacecom_1_1sun_1_1star_1_1util_1_1XPathSettings.html
    """
    if not name.strip():
        name = 'Work'
    path = _create_instance('com.sun.star.util.PathSettings', True)
    paths = getattr(path, name).split(';')
    for i, p in enumerate(paths):
        paths[i] = _system_path(p)
    if len(paths) == 1:
        return paths[0]
    return paths


def _properties(values):
    if values is None:
        return ()
    p = []
    if values:
        if isinstance(values, (tuple, list)):
            p = [PropertyValue(n, 0, v, 0) for n, v in values]
        elif isinstance(values, dict):
            p = [PropertyValue(n, 0, v, 0) for n, v in values.items()]
    return tuple(p)


def _set_properties(obj, properties):
    if PY2 or VERSION == '5.1':
        #~ REPORT - Need por OpenOffice, not apply setPropertyValues
        for k, v in properties.items():
            setattr(obj, k, v)
        return

    keys = tuple(properties.keys())
    values = tuple(properties.values())
    obj.setPropertyValues(keys, values)
    return


def _get_type_doc(obj):
    services = {
        'calc': 'com.sun.star.sheet.SpreadsheetDocument',
        'writer': 'com.sun.star.text.TextDocument',
        'impress': 'com.sun.star.presentation.PresentationDocument',
        'draw': 'com.sun.star.drawing.DrawingDocument',
        'base': 'com.sun.star.sdb.OfficeDatabaseDocument',
        'math': 'com.sun.star.formula.FormulaProperties',
        'basic': 'com.sun.star.script.BasicIDE',
    }
    for k, v in services.items():
        if obj.supportsService(v):
            return k
    return ''


def _get_class_doc(obj):
    classes = {
        'calc': LOCalc,
        'writer': LOWriter,
        'base': LOBase,
        'impress': LOImpress,
        'draw': LODraw,
        'math': LOMath,
        'basic': 'com.sun.star.script.BasicIDE',
    }
    type_doc = _get_type_doc(obj)
    return classes[type_doc](obj)


def _get_instance(name):
    services = {
        'label': 'com.sun.star.awt.UnoControlFixedTextModel',
        'button': 'com.sun.star.awt.UnoControlButtonModel',
        'text': 'com.sun.star.awt.UnoControlEditModel',
        'link': 'com.sun.star.awt.UnoControlFixedHyperlinkModel',
        'roadmap': 'com.sun.star.awt.UnoControlRoadmapModel',
        'listbox': 'com.sun.star.awt.UnoControlListBoxModel',
        'image': 'com.sun.star.awt.UnoControlImageControlModel',
        'groupbox': 'com.sun.star.awt.UnoControlGroupBoxModel',
        'radio': 'com.sun.star.awt.UnoControlRadioButtonModel',
        'tree': 'com.sun.star.awt.tree.TreeControlModel',
        'grid': 'com.sun.star.awt.grid.UnoControlGridModel',
    }
    return services[name.lower()]


def _get_custom_type(class_name):
    types = {
        'stardiv.Toolkit.UnoFixedTextControl': 'label',
        'stardiv.Toolkit.UnoButtonControl': 'button',
        'stardiv.Toolkit.UnoEditControl': 'text',
        'stardiv.Toolkit.UnoMultiPageControl': 'tab',
        'stardiv.Toolkit.UnoFixedHyperlinkControl': 'link',
        'stardiv.Toolkit.UnoRoadmapControl': 'roadmap',
        'stardiv.Toolkit.UnoListBoxControl': 'listbox',
        'stardiv.Toolkit.UnoImageControlControl': 'image',
        'stardiv.Toolkit.UnoGroupBoxControl': 'groupbox',
        'stardiv.Toolkit.UnoRadioButtonControl': 'radio',
        'stardiv.Toolkit.UnoTreeControl': 'tree',
        'stardiv.Toolkit.GridControl': 'grid',
        'com.sun.star.form.OFixedTextModel': 'label',
    }

    if not class_name in types:
        log.debug(class_name)
    return types.get(class_name, class_name)


def _to_date(value):
    new_value = value
    if isinstance(value, Time):
        new_value = datetime.time(value.Hours, value.Minutes, value.Seconds)
    elif isinstance(value, Date):
        new_value = datetime.date(value.Year, value.Month, value.Day)
    elif isinstance(value, DateTime):
        new_value = datetime.datetime(
            value.Year, value.Month, value.Day,
            value.Hours, value.Minutes, value.Seconds)
    return new_value


def _export_cvs(data, options):
    """
        See https://docs.python.org/3.5/library/csv.html#csv.writer
    """
    path = options['Path']
    options = options['Format']

    with open(path, 'w') as f:
        writer = csv.writer(f, **options)
        writer.writerows(data)
    return True


def _export_sqlite(data, options):
    import sqlite3

    conn = sqlite3.connect(options['Path'])
    table = options['Name']
    if options['Create']:
        drop = options.get('Drop', False)
        if drop:
            sql = 'DROP TABLE IF EXISTS {}'.format(table)
            conn.execute(sql)
        fields = []
        for i, f in enumerate(data[0]):
            if isinstance(data[1][i], float):
                fields.append('{} REAL'.format(f))
            else:
                fields.append('{} TEXT'.format(f))
        fields = ', '.join(fields)
        sql = '''CREATE TABLE IF NOT EXISTS {} (
            id INTEGER PRIMARY KEY AUTOINCREMENT, {}
        );'''.format(table, fields)
        conn.execute(sql)

    fields = data[0]
    data = data[1:]
    cursor = conn.cursor()
    sql = 'INSERT INTO {}({}) VALUES ({})'.format(
        table, ','.join(fields), ('?,' * len(fields))[:-1])
    cursor.executemany(sql, data)

    conn.commit()
    conn.close()
    return True


def _export_data(data, options):
    methods = {
        'csv': _export_cvs,
        'sqlite': _export_sqlite,
    }
    return methods[options['Type']](data, options)


class LODocument(object):
    DelimiterCharacter = '{{{}}}'
    CaseSensitive = False
    RegularExpression = False
    SearchWords = False

    def __init__(self, obj):
        self._obj = obj
        self._type_doc = _get_type_doc(obj)
        self._cc = self._obj.getCurrentController()
        #~ Not work, I'll report
        self.sb = self._cc.StatusIndicator

    @property
    def obj(self):
        return self._obj

    @property
    def type(self):
        return self._type_doc

    @property
    def title(self):
        return self._obj.getTitle()

    @property
    def is_saved(self):
        return self._obj.hasLocation()

    @property
    def is_modified(self):
        return self._obj.isModified()

    @property
    def is_read_only(self):
        return self._obj.isReadOnly()

    @property
    def path(self):
        return _system_path(self._obj.getURL())

    @property
    def visible(self):
        w = self._cc.getFrame().getContainerWindow()
        return w.Visible
    @visible.setter
    def visible(self, value):
        w = self._cc.getFrame().getContainerWindow()
        w.setVisible(value)

    @property
    def zoom(self):
        return self._cc.ZoomValue
    @zoom.setter
    def zoom(self, value):
        self._cc.ZoomValue = value

    def save(self, path='', opt=None):
        path = _file_url(path)
        opt = _properties(opt)
        if path:
            self._obj.storeAsURL(path, opt)
        else:
            self._obj.store()
        return True

    def to_pdf(self, path='', opt=None):
        """
            Export to PDF
            http://wiki.services.openoffice.org/wiki/API/Tutorials/PDF_export
        """
        if path:
            path_save = path
            if os.path.isdir(path):
                name = self._obj.getURL().split('/')[-1][:-4] + '.pdf'
                path_save = os.path.join(path, name)
        else:
            path_save = self.path[:-4] + '.pdf'

        path_save = _file_url(path_save)
        filter_name = '{}_pdf_Export'.format(self.type)
        if opt:
            filter_data = _properties(opt)
            filter_options = (
                ('FilterName', filter_name),
                ('FilterData', filter_data),
            )
            #~ uno.Any('[]com.sun.star.beans.PropertyValue', filter_data)
        else:
            filter_options = (('FilterName', filter_name),)
        media_descriptor = _properties(filter_options)
        self._obj.storeToURL(path_save, media_descriptor)
        return _system_path(path_save)

    def focus(self):
        w = self._cc.getFrame().getComponentWindow()
        w.setFocus()
        return

    def close(self):
        self._obj.close(True)
        return


class LOCalcSheets(object):

    def __init__(self, obj, parent):
        self._obj = obj
        self._parent = parent

    def __getitem__(self, index):
        if PY2:
            return self._get_item(index)
        return self._get_sheet(self._obj[index])

    def _get_item(self, index):
        if isinstance(index, int):
            sheet = self._obj.getByIndex(index)
        else:
            sheet = self._obj.getByName(index)
        return self._get_sheet(sheet)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        pass

    def _get_sheet(self, sheet):
        return LOCalcSheet(sheet, self._parent)

    @property
    def obj(self):
        return self._obj

    @property
    def names(self):
        return self._obj.getElementNames()

    def index(self, name):
        return self.names.index(name)

    def exists(self, name):
        return name in self.names

    def insert(self, name, pos=-1):
        if pos == -1:
            pos = len(self.names)
        self._obj.insertNewByName(name, pos)
        return self._get_sheet(self._obj[pos])

    def remove(self, name):
        if isinstance(name, int):
            name = self.names[name]
        self._obj.removeByName(name)
        return True

    def move(self, name, pos=-1):
        if pos == -1:
            pos = len(self.names)
        if isinstance(name, int):
            name = self.names[name]
        self._obj.moveByName(name, pos)
        return True

    def copy(self, source, new_name, pos=-1):
        if pos == -1:
            pos = len(self.names)
        source_name = self._obj[source].Name
        self._obj.copyByName(source_name, new_name, pos)
        return self._get_sheet(self._obj[pos])

    def import_sheet(self, source, name, pos=-1):
        if pos == -1:
            pos = len(self.names)
        if isinstance(name, int):
            name = source.sheets[name].name
        new_pos = self._obj.importSheet(source.obj, name, pos)
        return self._get_sheet(self._obj[new_pos])


class LOForm(object):

    def __init__(self, obj):
        self._obj = obj

    def __getitem__(self, index):
        if isinstance(index, int):
            control = self._obj.getByIndex(index)
        else:
            control = self._obj.getByName(index)
        #~ return _get_custom_class('', control)
        return control

    @property
    def name(self):
        return self._obj.getName()
    @name.setter
    def name(self, value):
        self._obj.setName(value)

    @property
    def data(self):
        info = {
            'Source': self._obj.DataSourceName,
            'Type': self._obj.CommandType,
            'Command': self._obj.Command,
        }
        return info
    @data.setter
    def data(self, value):
        self._obj.DataSourceName = value['Source']
        self._obj.CommandType = value['Type']
        self._obj.Command = value['Command']

    @property
    def obj(self):
        return self._obj


class LOForms(object):

    def __init__(self, obj):
        self._obj = obj
        self._doc = self._obj.getParent()

    def __getitem__(self, index):
        if isinstance(index, int):
            form = self._obj.getByIndex(index)
        else:
            form = self._obj.getByName(index)
        return LOForm(form)

    @property
    def obj(self):
        return self._obj

    @property
    def count(self):
        return self._obj.getCount()

    @property
    def names(self):
        return self._obj.getElementNames()

    def exists(self, name):
        return self._obj.hasByName(name)

    def add(self, index):
        new = self._doc.createInstance('com.sun.star.form.component.Form')
        if isinstance(index, int):
            self._obj.insertByIndex(index, new)
            form = self._obj.getByIndex(index)
        else:
            self._obj.insertByName(index, new)
            form = self._obj.getByName(index)
        return LOForm(form)

    def delete(self, index):
        if isinstance(index, int):
            self._obj.removeByIndex(index)
        else:
            self._obj.removeByName(index)
        return


class LOCalcSheet(object):

    def __init__(self, obj, parent):
        self._obj = obj
        self._parent = parent
        self._dp = self._obj.getDrawPage()
        self._images = {i.Anchor.AbsoluteName: LOImage(i) for i in self._dp}

    def __getitem__(self, index):
        if PY2:
            return self._get_item(index)
        return self._get_range(self._obj[index])

    #~ Delete when OOo used Py3
    def _get_item(self, index):
        if isinstance(index, str):
            rango = self._obj.getCellRangeByName(index)
            return self._get_range(rango)

        if isinstance(index, int):
            cols = self._obj.getColumns().getCount() - 1
            rango = self._obj.getCellRangeByPosition(0, index, cols, index)
            return self._get_range(rango)

        rows = index[0]
        columns = index[1]

        if isinstance(rows, int) and isinstance(columns, int):
            rango = self._obj.getCellByPosition(*index)
            return self._get_range(rango)

        if isinstance(rows, int):
            row1 = row2 = rows
        else:
            row1 = rows.start or 0
            if rows.stop is None:
                row2 = self._obj.getRows().getCount() - 1
            else:
                row2 = rows.stop - 1

        if isinstance(columns, int):
            col1 = col2 = columns
        else:
            col1 = columns.start or 0
            if columns.stop is None:
                col2 = self._obj.getColumns().getCount() - 1
            else:
                col2 = rows.stop - 1

        rango = self._obj.getCellRangeByPosition(col1, row1, col2, row2)
        return self._get_range(rango)

    def _get_range(self, rango):
        return LOCellRange(rango, self._parent.path)

    @property
    def obj(self):
        return self._obj

    @property
    def name(self):
        return self._obj.Name
    @name.setter
    def name(self, value):
        self._obj.setName(value)

    @property
    def visible(self):
        return self._obj.IsVisible
    @visible.setter
    def visible(self, value):
        self._obj.IsVisible = value

    @property
    def color(self):
        return self._obj.TabColor
    @visible.setter
    def color(self, value):
        self._obj.TabColor = _color(value)

    @property
    def is_protected(self):
        return self._obj.isProtected()

    def protect(self, password=''):
        self._obj.protect(password)
        return

    def unprotect(self, password=''):
        self._obj.unprotect(password)
        return

    @property
    def images(self):
        return self._images

    @property
    def forms(self):
        return LOForms(self._dp.getForms())


class LOCellRange(object):

    def __init__(self, obj, path):
        self._obj = obj
        self._path = path
        self._type_obj = obj.ImplementationName
        self._is_cell = False
        self._is_ranges = False
        self._type_content = self._get_type_content()
        self._init_values()

    def _get_range(self, rango):
        return LOCellRange(rango, self._path)

    def __getitem__(self, index):
        if PY2:
            return self._get_item(index)
        return self._get_range(self._obj[index])

    def _get_item(self, index):
        if isinstance(index, str):
            rango = self._obj.getCellRangeByName(index)
            return self._get_range(rango)

        if isinstance(index, int):
            cols = self._obj.getColumns().getCount() - 1
            rango = self._obj.getCellRangeByPosition(0, index, cols, index)
            return self._get_range(rango)

        rows = index[0]
        columns = index[1]

        if isinstance(rows, int) and isinstance(columns, int):
            rango = self._obj.getCellByPosition(columns, rows)
            return self._get_range(rango)

        if isinstance(rows, int):
            row1 = row2 = rows
        else:
            row1 = rows.start or 0
            if rows.stop is None:
                row2 = self._obj.getRows().getCount() - 1
            else:
                row2 = rows.stop - 1

        if isinstance(columns, int):
            col1 = col2 = columns
        else:
            col1 = columns.start or 0
            if columns.stop is None:
                col2 = self._obj.getColumns().getCount() - 1
            else:
                col2 = rows.stop - 1

        rango = self._obj.getCellRangeByPosition(col1, row1, col2, row2)
        return self._get_range(rango)

    def _init_values(self):
        if self._is_ranges:
            self._address = self._obj.AbsoluteName.split(',')
        else:
            self._address = self._obj.AbsoluteName
        return

    @property
    def obj(self):
        return self._obj

    @property
    def type(self):
        return self._type_content

    def _get_type_content(self):
        #~ Default ScCellRangeObj
        tc = ''
        if self._type_obj == 'ScCellObj':
            tc = self._obj.getType()
            self._is_cell = True
        elif self._type_obj == 'ScCellRangesObj':
            self._is_ranges = True
        return tc

    def query(self, args):
        base = LOBase()
        base.connect(args.name, args.user, args.password)
        rows = base.query(args.sql)
        self.data = rows
        return rows

    @property
    def is_cell(self):
        return self._is_cell

    @property
    def first(self):
        return self._get_range(self._obj[0,0])

    @property
    def unique(self):
        return tuple(set(self._obj.getDataArray()))

    @property
    def data(self):
        return self._obj.getDataArray()
    @data.setter
    def data(self, rows):
        sheet = self.sheet
        if PY2:
            ca = self._obj.getCellByPosition(0,0).getCellAddress()
        else:
            ca = self._obj[0,0].getCellAddress()
        pos = (ca.Column, ca.Row,
            ca.Column + len(rows[0]) - 1, ca.Row + len(rows) - 1)
        rango = sheet.getCellRangeByPosition(*pos)
        if isinstance(rows, list):
            rows = tuple(rows)
        rango.setDataArray(rows)

    @property
    def data_column(self):
        return tuple(zip(*self.data))[0]

    @property
    def address(self):
        return self._address

    @property
    def range_address(self):
        return self._obj.getRangeAddress()

    @property
    def cell_address(self):
        if PY2:
            return self._obj.getCellByPosition(0,0).getCellAddress()
        return self._obj[0,0].getCellAddress()

    @property
    def sheet(self):
        return self._obj.getSpreadsheet()

    def clear(self, value=31):
        #~ http://api.libreoffice.org/docs/idl/ref/namespacecom_1_1sun_1_1star_1_1sheet_1_1CellFlags.html
        self._obj.clearContents(value)
        return

    def fill(self, direction=0, rows=1):
        self._obj.fillAuto(direction, rows)
        return

    @property
    def value(self):
        v = None
        if self._type_content == VALUE:
            v = self._obj.getValue()
        elif self._type_content == TEXT:
            v = self._obj.getString()
        elif self._type_content == FORMULA:
            v = self._obj.getFormula()
        return v
    @value.setter
    def value(self, data):
        if isinstance(data, str):
            if data.startswith('='):
                self._obj.setFormula(data)
            else:
                self._obj.setString(data)
        elif isinstance(data, (int, float)):
            self._obj.setValue(data)

    @property
    def string(self):
        return self._obj.String
    @string.setter
    def string(self, data):
        self._obj.setString(data)

    @property
    def current_region(self):
        sheet = self.sheet
        if PY2:
            cursor = sheet.createCursorByRange(self._obj.getCellByPosition(0,0))
        else:
            cursor = sheet.createCursorByRange(self._obj[0,0])
        cursor.collapseToCurrentRegion()

        if PY2:
            return self._get_range(sheet.getCellRangeByName(cursor.AbsoluteName))

        return self._get_range(sheet[cursor.AbsoluteName])

    def offset(self, col=1, row=0):
        sheet = self.sheet
        if PY2:
            cursor = sheet.createCursorByRange(self._obj.getCellByPosition(0,0))
        else:
            cursor = sheet.createCursorByRange(self._obj[0,0])
        cursor.gotoOffset(col, row)

        if PY2:
            return self._get_range(sheet.getCellRangeByName(cursor.AbsoluteName))

        return self._get_range(sheet[cursor.AbsoluteName])

    def move(self, target, headers=True):
        sheet = self.sheet
        source = self.range_address
        if headers:
            source.StartRow += 1
        sheet.moveRange(target.cell_address, source)
        return

    def copy(self, target):
        sheet = self.sheet
        sheet.copyRange(target.cell_address, self.range_address)
        return

    def copy_data(self, target):
        sheet = target.sheet
        ra = target.range_address
        data = self.data
        pos = (ra.StartColumn, ra.StartRow,
            ra.StartColumn + len(data[0]) - 1, ra.StartRow + len(data) - 1)
        rango = sheet.getCellRangeByPosition(*pos)
        rango.setDataArray(data)
        return

    @property
    def columns(self):
        return self._obj.getColumns()

    @property
    def rows(self):
        return self._obj.getRows()

    def optimal_width(self):
        self.current_region.columns.OptimalWidth = True
        return

    @property
    def back_color(self):
        return self._obj.CellBackColor
    @back_color.setter
    def back_color(self, value):
        self._obj.CellBackColor = _color(value)

    @property
    def visibles(self):
        return self._obj.queryVisibleCells()

    @property
    def empty(self):
        return self._obj.queryEmptyCells()

    def get_content(self, value=1):
        return self._obj.queryContentCells(value)

    def set_visible(self, show):
        self._obj.getRows().IsVisible = show
        return

    @property
    def row(self):
        if self._is_cell:
            return self.cell_address.Row
        ra = self.range_address
        return (ra.StartRow, ra.EndRow)

    @property
    def column(self):
        if self._is_cell:
            return self.cell_address.Column
        ra = self.range_address
        return (ra.StartColumn, ra.EndColumn)

    def set_string(self):
        columns = self.columns.Count
        rows = self.rows.Count
        data = []
        for r in range(rows):
            row = []
            for c in range(columns):
                cell = self._obj.getCellByPosition(c, r)
                row.append(cell.String)
            data.append(tuple(row))
        self.data = tuple(data)
        return

    @property
    def note(self):
        return self.first._obj.getAnnotation().String

    @note.setter
    def note(self, value):
        n = self.first._obj.getAnnotation()
        if n.AnnotationShape is None:
            notes = self.sheet.getAnnotations()
            notes.insertNew(self.cell_address, value)
        else:
            n.setString(value)

    @property
    def note_shape(self):
        return self.first._obj.getAnnotation().AnnotationShape

    def note_show(self, value):
        n = self.first._obj.getAnnotation()
        if not n.AnnotationShape is None:
            n.setIsVisible(value)
        return

    def note_delete(self):
        notes = self.sheet.getAnnotations()
        for i, note in enumerate(notes):
            if note.Position == self.cell_address:
                notes.removeByIndex(i)
                break
        return

    def get_next_cell(self):
        sheet = self.sheet
        ra = self.current_region.range_address
        if PY2:
            cell = sheet.getCellByPosition(ra.StartColumn, ra.EndRow + 1)
        else:
            cell = sheet[ra.EndRow + 1, ra.StartColumn]
        return self._get_range(cell)

    @property
    def next_row(self):
        cr = self.current_region
        return cr._obj.getRangeAddress().EndRow + 1

    def export(self, options={}):
        options = self._default_export_options(options)
        data = self.data
        if not options['Headers']:
            data = data[1:]
        _export_data(data, options)
        return

    def _default_export_options(self, options):
        path, filename = os.path.split(self._path)
        name, _ = os.path.splitext(filename)
        ext = options.get('Type', 'csv')
        if ext == 'csv':
            name = self.sheet.Name
        path = os.path.join(path, '{}.{}'.format(name, ext))
        default = {
            'Type': ext,
            'Path': path,
            'Headers': True,
            'Format': {},
            'Name': self.sheet.Name,
        }
        for k, v in default.items():
            if not k in options:
                options[k] = v
        return options

    def to_dict(self):
        rows = self.data
        fields = rows[0]
        rows = [dict(zip(fields, row)) for row in rows[1:]]
        return rows


class LOCalc(LODocument):

    def __init__(self, obj):
        super(LOCalc, self).__init__(obj)
        self.sheets = LOCalcSheets(self._obj.getSheets(), obj)

    def _get_class_selection(self, selection):
        classes = {
            'ScCellObj': LOCellRange,
            'ScCellRangeObj': LOCellRange,
            'ScCellRangesObj': LOCellRange,
        }
        class_name = selection.ImplementationName
        if class_name in classes:
            return classes[class_name](selection, self.path)
        log.debug(selection.ImplementationName)
        #~ com.sun.star.drawing.SvxShapeCollection
        return selection

    @property
    def selection(self):
        return self._get_class_selection(self._obj.getCurrentSelection())

    @property
    def headers(self):
        return self._cc.ColumnRowHeaders
    @headers.setter
    def headers(self, value):
        self._cc.ColumnRowHeaders = value

    @property
    def zoom(self):
        return self._cc.ZoomValue
    @zoom.setter
    def zoom(self, value):
        self._cc.ZoomValue = value

    @property
    def active(self):
        return LOCalcSheet(self._cc.getActiveSheet(), self)

    def set_active(self, sheet):
        self._cc.setActiveSheet(sheet.obj)
        return

    def select(self, cells):
        self._cc.select(cells.obj)
        return

    def filter_by_color(self, cell=None):
        if cell is None:
            cell = self.selection.first
        visibles = cell.column.visibles
        for c in visibles.getCells():
            if c.CellBackColor != cell.back_color:
                c.getRows().IsVisible = False
        return

    def rows_show(self, cell=None):
        if cell is None:
            cells = self.selection.current_region
        else:
            cells = cell.current_region
        cells.set_visible(True)
        return

    def export(self, options):
        return


class LOWriterText(object):

    def __init__(self, obj, parent=None):
        self._obj = obj
        self._parent = parent

    @property
    def obj(self):
        return self._obj

    @property
    def style(self):
        return list(self._obj)[0].ParaStyleName
    @style.setter
    def style(self, value):
        for p in self._obj:
            p.ParaStyleName = value

    def set_property(self, name, value):
        for p in self._obj:
            p.setPropertyValue(name, value)
        return


class LOWriter(LODocument):

    def __init__(self, obj):
        super(LOWriter, self).__init__(obj)
        self._text = self.text
        self._style_families = self._obj.getStyleFamilies()

    @property
    def selection(self):
        return self._obj.getCurrentSelection()

    @property
    def text(self):
        return LOWriterText(self._obj.Text, self)

    def insert_character(self, character=PARAGRAPH_BREAK, cursor=None):
        if cursor is None:
            cursor = self._text.getEnd()
        self._text.insertControlCharacter(cursor, character, False)
        return

    def get_style(self, name, style_type='Paragraph'):
        sf = self._style_families['{}Styles'.format(style_type)]
        return sf[name]

    def insert_style(self, name, format_style, style_type='Paragraph'):
        if name in self._style_families['{}Styles'.format(style_type)]:
            return
        sf = self._style_families['{}Styles'.format(style_type)]
        new_style = self._obj.createInstance(
            'com.sun.star.style.{}Style'.format(style_type))
        sf.insertByName(name, new_style)
        new_style.setPropertyValues(
            tuple(format_style.keys()), tuple(format_style.values()))
        return new_style

    def set_style(self, name, cursor=None, style_type='Paragraph'):
        #~ style_type = Character, Paragraph, Page, Frame, Numbering, Table
        if not name in self._style_families['{}Styles'.format(style_type)]:
            return
        if cursor is None:
            cursor = self._text.createTextCursor()
            cursor.gotoEnd(False)
        if style_type == 'Paragraph':
            cursor.ParaStyleName = name
        return

    def find(self, search, first=False):
        sd = self._obj.createSearchDescriptor()
        sd.SearchCaseSensitive = self.CaseSensitive
        sd.SearchRegularExpression = self.RegularExpression
        sd.SearchWords = self.SearchWords
        sd.setSearchString(search)
        if first:
            return self._obj.findFirst(sd)
        else:
            return self._obj.findAll(sd)

    def replace(self, search, replace):
        sd = self._obj.createReplaceDescriptor()
        sd.SearchCaseSensitive = self.CaseSensitive
        sd.SearchRegularExpression = self.RegularExpression
        sd.SearchWords = self.SearchWords
        sd.setSearchString(search)
        sd.setReplaceString(replace)
        return self._obj.replaceAll(sd)

    def render(self, data, clean=True):
        if isinstance(data, dict):
            data = data.items()
        for s, r in data:
            search = self.DelimiterCharacter.format(s)
            replace = r
            if r is None and clean:
                replace = ''
            self.replace(search, replace)
        return

    @property
    def string(self):
        return self._obj.Text.String

    @string.setter
    def string(self, value):
        if isinstance(value, str):
            self._obj.Text.String = value
        else:
            text = self.text
            cursor = text.createTextCursor()
            for v, s in value:
                text.insertString(text.getEnd(), v, False)
                if s:
                    cursor.gotoEnd(False)
                    self.set_style(s, cursor)
                self.insert_character()

    def write(self, info):
        text = self._obj.Text
        cursor = text.createTextCursor()
        cursor.gotoEnd(False)
        text.insertString(cursor, str(info), 0)
        return


class LOImpress(LODocument):

    def __init__(self, obj):
        super(LOImpress, self).__init__(obj)


class LODraw(LODocument):

    def __init__(self, obj):
        super(LODraw, self).__init__(obj)


class LOMath(LODocument):

    def __init__(self, obj):
        super(LOMath, self).__init__(obj)


class LOImage(object):

    def __init__(self, obj):
        self._obj = obj

    @property
    def url(self):
        return self._obj.GraphicURL

    @url.setter
    def url(self, value):
        self._obj.GraphicURL = value

    @property
    def visible(self):
        return self._obj.Visible
    @visible.setter
    def visible(self, value):
        self._obj.Visible = value



class ListenerBase(unohelper.Base, XEventListener):

    def __init__(self, controller, window=None):
        self._controller = controller
        self._window = window

    def disposing(self, event):
        self._controller = None
        if not self._window is None:
            self._window.setMenuBar(None)


class EventsWindow(ListenerBase, XWindowListener):

    def __init__(self, controller, container):
        super(EventsWindow, self).__init__(controller)
        self._container = container

    @catch_exception
    def windowResized(self, event):
        # ~ sb = self._container.getControl('subcontainer')
        # ~ sb.setPosSize(0, 0, event.Width, event.Height, SIZE)
        control_name = 'window_resized'
        if hasattr(self._controller, control_name):
            getattr(self._controller, control_name)(event)
        return

    def windowMoved(self, event):
        pass

    def windowShown(self, event):
        pass

    def windowHidden(self, event):
        pass


class EventsTopWindow(ListenerBase, XTopWindowListener):

    def __init__(self, controller, modal=True, block=False, window=None):
        super(EventsTopWindow, self).__init__(controller, window)
        self._modal = modal
        self._block = block

    def windowOpened(self, event):
        control_name = '{}_opened'.format(event.Source.Model.Name)
        if hasattr(self._controller, control_name):
            getattr(self._controller, control_name)(event)
        return

    def windowActivated(self, event):
        control_name = '{}_activated'.format(event.Source.Model.Name)
        if hasattr(self._controller, control_name):
            getattr(self._controller, control_name)(event)
        return

    def windowDeactivated(self, event):
        control_name = '{}_deactivated'.format(event.Source.Model.Name)
        if hasattr(self._controller, control_name):
            getattr(self._controller, control_name)(event)
        return

    def windowMinimized(self, event):
        pass

    def windowNormalized(self, event):
        pass

    @catch_exception
    def windowClosing(self, event):
        if self._window:
            control_name = 'window_closing'
        else:
            control_name = '{}_closing'.format(event.Source.Model.Name)

        if hasattr(self._controller, control_name):
            getattr(self._controller, control_name)(event)
        else:
            if not self._modal and not self._block:
                event.Source.Visible = False
        return

    def windowClosed(self, event):
        control_name = '{}_closed'.format(event.Source.Model.Name)
        if hasattr(self._controller, control_name):
            getattr(self._controller, control_name)(event)
        return


class EventsMenu(ListenerBase, XMenuListener):

    def __init__(self, controller):
        super(EventsMenu, self).__init__(controller)

    def itemHighlighted(self, event):
        pass

    def itemSelected(self, event):
        cmd = '{}_selected'.format(event.Source.getCommand(event.MenuId))
        if hasattr(self._controller, cmd):
            getattr(self._controller, cmd)(event)
        return

    def itemActivated(self, event):
        return

    def itemDeactivated(self, event):
        return


class EventsKey(ListenerBase, XKeyListener):

    def __init__(self, controller):
        super(EventsKey, self).__init__(controller)

    def keyPressed(self, event):
        pass

    def keyReleased(self, event):
        pass


class EventsButton(ListenerBase, XActionListener):

    def __init__(self, controller):
        super(EventsButton, self).__init__(controller)

    def _custom_action(self, name):
        if hasattr(self._controller, name):
            control = getattr(self._controller, name)
            if isinstance(control, UnoRadio):
                if not control.parent is None:
                    control.parent.value = control.id
        return

    def actionPerformed(self, event):
        name = event.Source.Model.Name
        event_name = '{}_action'.format(name)
        # ~ self._custom_action(name)
        if hasattr(self._controller, event_name):
            getattr(self._controller, event_name)(event)
        return


class EventsMouse(ListenerBase, XMouseListener):

    def __init__(self, controller):
        super(EventsMouse, self).__init__(controller)

    def mousePressed(self, event):
        event_name = '{}_click'.format(event.Source.Model.Name)
        if event.ClickCount == 2:
            event_name = '{}_doubleclick'.format(event.Source.Model.Name)
        if hasattr(self._controller, event_name):
            getattr(self._controller, event_name)(event)
        return

    def mouseReleased(self, event):
        pass

    def mouseEntered(self, event):
        pass

    def mouseExited(self, event):
        pass


class EventsMouseLink(EventsMouse):

    def __init__(self, controller):
        super(EventsMouseLink, self).__init__(controller)

    def mouseEntered(self, event):
        obj = event.Source.Model
        obj.TextColor = COLORS['BLUE']
        return

    def mouseExited(self, event):
        obj = event.Source.Model
        obj.TextColor = 0
        return


class EventsTab(ListenerBase, XTabListener):

    def __init__(self, controller, name):
        super(EventsTab, self).__init__(controller)
        self._name = name

    def activated(self, ID):
        control_name = '{}_activated'.format(self._name)
        if hasattr(self._controller, control_name):
            getattr(self._controller, control_name)(ID)
        return


class EventsFocus(ListenerBase, XFocusListener):

    def __init__(self, controller, color=COLORS['INFOCUS']):
        super(EventsFocus, self).__init__(controller)
        self._color = color

    def focusGained(self, event):
        event_name = '{}_focus_gained'.format(event.Source.Model.Name)
        if hasattr(self._controller, event_name):
            getattr(self._controller, event_name)(event)
        else:
            obj = event.Source.Model
            obj.Border = 0
            obj.BackgroundColor = self._color
        return

    def focusLost(self, event):
        event_name = '{}_focus_lost'.format(event.Source.Model.Name)
        if hasattr(self._controller, event_name):
            getattr(self._controller, event_name)(event)
        else:
            obj = event.Source.Model
            obj.Border = 1
            obj.BackgroundColor = COLORS['DEFAULT']
        return


class EventsItem(ListenerBase, XItemListener):

    def __init__(self, controller):
        super(EventsItem, self).__init__(controller)

    def itemStateChanged(self, event):
        pass


class EventsSelectionChange(ListenerBase, XSelectionChangeListener):

    def __init__(self, controller):
        super(EventsSelectionChange, self).__init__(controller)

    def selectionChanged(self, event):
        control_name = '{}_selection_changed'.format(event.Source.Model.Name)
        if hasattr(self._controller, control_name):
            getattr(self._controller, control_name)(event)


class EventsRoadmap(EventsItem):

    def itemStateChanged(self, event):
        control_name = '{}_item_changed'.format(event.Source.Model.Name)
        if hasattr(self._controller, control_name):
            getattr(self._controller, control_name)(event)
        else:
            dlg = event.Source.Context
            dlg.Model.Step = event.ItemId + 1
        return


class UnoBaseObject(object):

    def __init__(self, obj, label=None):
        self._obj = obj
        self._label = label
        self._model = self._obj.Model
        self._name = self._model.Name
        self._value = None
        self._data = None
        self._rules = {'empty': False}
        self._field = ''
        self._format = '{}'
        self._next = ''
        self._type = self._get_type()
        self._in_form = False

    def _get_type(self):
        return _get_custom_type(self._obj.ImplementationName)

    def config(self, properties):
        for k, v in properties.items():
            if hasattr(self._obj, k):
                setattr(self._obj, k, v)
        return

    def set_focus(self):
        self._obj.setFocus()
        return

    def move(self, target, direction='B', X=_SEPARATION_X, Y=_SEPARATION_Y):
        if direction == 'L':
            x = target.x + X + target.width
            y = target.y
        elif direction == 'B':
            x = target.x
            y = target.y + Y + target.height

        self.x = x
        self.y = y

        if self._label:
            self._label.x = x - self._label.width - _SEPARATION_X
            self._label.y = y

        return

    @property
    def tag(self):
        return self._model.Tag
    @tag.setter
    def tag(self, value):
        self._model.Tag = value

    @property
    def step(self):
        return self._model.Step
    @step.setter
    def step(self, value):
        self._model.Step = value

    @property
    def x(self):
        return self._model.PositionX
    @x.setter
    def x(self, value):
        self._model.PositionX = value

    @property
    def y(self):
        return self._model.PositionY
    @y.setter
    def y(self, value):
        self._model.PositionY = value

    @property
    def width(self):
        return self._model.Width
    @width.setter
    def width(self, value):
        self._model.Width = value

    @property
    def height(self):
        return self._model.Height
    @height.setter
    def height(self, value):
        self._model.Height = value

    @property
    def obj(self):
        return self._obj

    @property
    def name(self):
        return self._name

    @property
    def parent(self):
        return self._obj.getContext()


class UnoLabel(UnoBaseObject):

    def __init__(self, obj, label):
        super(UnoLabel, self).__init__(obj, label)

    @property
    def value(self):
        return self._model.Label
    @value.setter
    def value(self, value):
        self._model.Label = value


class UnoLink(UnoBaseObject):

    def __init__(self, obj, label):
        super(UnoLink, self).__init__(obj, label)


class UnoRadio(UnoBaseObject):

    def __init__(self, obj, label):
        super(UnoRadio, self).__init__(obj, label)
        self._id = -1
        self._parent = None

    @property
    def value(self):
        return bool(self._model.State)
    @value.setter
    def value(self, value):
        self._model.State = value

    @property
    def id(self):
        return self._id
    @id.setter
    def id(self, value):
        self._id = value

    @property
    def parent(self):
        return self._parent
    @parent.setter
    def parent(self, value):
        self._parent = value


class UnoText(UnoBaseObject):

    def __init__(self, obj, label):
        super(UnoText, self).__init__(obj, label)

    @property
    def value(self):
        return self._model.Text
    @value.setter
    def value(self, value):
        self._model.Text = value


class UnoRoadmap(UnoBaseObject):

    def __init__(self, obj, label):
        super(UnoRoadmap, self).__init__(obj, label)


class UnoGrid(UnoBaseObject):

    def __init__(self, obj, label):
        super(UnoGrid, self).__init__(obj, label)
        self._gdm = self._model.GridDataModel
        self._format_columns = ()

    def _format_cols(self):
        rows = tuple(tuple(
            self._format_columns[i].format(r) for i, r in enumerate(row)) for row in self._data
        )
        return rows

    @property
    def format_columns(self):
        return self._format_columns
    @format_columns.setter
    def format_columns(self, value):
        self._format_columns = value

    @property
    def data(self):
        return self._data
    @data.setter
    def data(self, value):
        self._data = value
        self._gdm.removeAllRows()
        heading = tuple(range(1, len(value) + 1))
        if self._format_columns:
            values = self._format_cols()
        self._gdm.addRows(heading, values)
        return


class UnoTree(UnoBaseObject):

    def __init__(self, obj, label):
        super(UnoTree, self).__init__(obj, label)
        self._parents = {}
        self._is_path = False
        self.save_files = True
        self.filter_ext = ''
        self._files = {}

    @property
    def files(self):
        sel = self.selection
        return self._files[sel.DataValue]

    @property
    def selection(self):
        return self._obj.Selection

    @property
    def value(self):
        if self._is_path:
            sel = self.selection
            data = [self._tdm.Root.DisplayValue]
            while sel.DataValue:
                data.insert(1, sel.DisplayValue)
                sel = sel.getParent()
            return os.path.join(*data)

        return self.selection.DisplayValue

    @property
    def data(self):
        return self._data
    @data.setter
    def data(self, values):
        if isinstance(values, str) and os.path.isdir(values):
            self._get_folders(values)
            self._is_path = True
        else:
            self._data = values
        self._add_data()

    def _filter_files(self, files):
        if not self.filter_ext:
            return files

        template = r'\.{}'
        if isinstance(self.filter_ext, tuple):
            template += (r'|\.{}' * (len(self.filter_ext) - 1))
            pattern = re.compile(template.format(*self.filter_ext), re.IGNORECASE)
        else:
            pattern = re.compile(template.format(self.filter_ext), re.IGNORECASE)
        return [f for f in files if pattern.search(f)]

    def _get_folders(self, path):
        self._add_data_model(path)
        _, root = os.path.split(path)
        parents = {}
        data = []
        id = 0
        for path, names, files in os.walk(path):
            _, pn = os.path.split(path)
            parent = parents.get(pn, 0)
            self._files[parent] = self._filter_files(files)
            for name in names:
                id += 1
                data.append((id, parent, name))
                parents[name] = id
        self._data = tuple(data)
        return

    def _add_data(self):
        for node in self._data:
            parent = self._parents.get(node[1], self._tdm.Root)
            child = self._tdm.createNode(node[2], False)
            child.DataValue = node[0]
            parent.appendChild(child)
            self._parents[node[0]] = child
        self._obj.expandNode(self._tdm.Root)
        self._parents = {}
        return

    def _add_data_model(self, name=''):
        tdm = _create_instance('com.sun.star.awt.tree.MutableTreeDataModel')
        root = tdm.createNode(name, True)
        root.DataValue = 0
        tdm.setRoot(root)
        self._model.DataModel = tdm
        self._tdm = self._model.DataModel
        return

    @property
    def root(self):
        try:
            return self._tdm.Root.DisplayValue
        except:
            return ''
    @root.setter
    def root(self, value):
        self._add_data_model(value)


class UnoListBox(UnoBaseObject):

    def __init__(self, obj, label):
        super(UnoListBox, self).__init__(obj, label)

    @property
    def data(self):
        return self._data
    @data.setter
    def data(self, values):
        self._data = values
        if isinstance(values, list):
            values = tuple(sorted(values))
        if PY2:
            self._model.StringItemList = uno.Any('[]string', values)
        else:
            self._model.StringItemList = values
        return


class UnoButton(UnoBaseObject):

    def __init__(self, obj, label):
        super(UnoButton, self).__init__(obj, label)
        self._set_icon()

    def _set_icon(self):
        icon_name = self.tag.strip()
        if icon_name:
            path_icon = _file_url('{}/img/{}'.format(CURRENT_PATH, icon_name))
            self._model.ImageURL = path_icon
            if self.value:
                self._model.ImageAlign = 0
        return

    @property
    def value(self):
        return self._model.Label
    @value.setter
    def value(self, value):
        self._model.Label = value


class UnoImage(UnoBaseObject):

    def __init__(self, obj, label):
        super(UnoImage, self).__init__(obj, label)
        self._set_image()

    def _set_image(self):
        image = self.tag.strip()
        if image:
            path = _file_url('{}/img/{}'.format(CURRENT_PATH, image))
            self._model.ImageURL = path
        return

    @property
    def center(self):
        return self.width / 2, self.height / 2
    @center.setter
    def center(self, value):
        self.x = value[0] - (self.width / 2)
        self.y = value[1] - (self.height / 2)
        return


class UnoGroupBox(UnoBaseObject):

    def __init__(self, obj, label):
        super(UnoGroupBox, self).__init__(obj, label)
        self._options = {}
        self._direction = 'L'
        self._value = -1

    def _set_properties(self, opt):
        default = {
            'Type': 'radio',
            'Step': self.step,
            'PositionX': self.x + _SEPARATION_X,
            'PositionY': self.y + _SEPARATION,
            'Custom': {'parent': self, 'id': 0},
        }
        if isinstance(opt, str):
            default['Label'] = opt
            default['Name'] = 'opt_{}'.format(opt.lower().replace(' ', '_'))
        return default

    def add_options(self, options, dlg):
        if not options.get('Horizontal', True):
            self._direction = 'B'
        default = options.get('Default', -1)
        options = options['Options']

        last = False
        for i, opt in enumerate(options):
            properties = self._set_properties(opt)
            properties['Custom']['id'] = i
            control = dlg.add_control(properties)
            self._options[i] = control
            if not last:
                last = control
                continue
            control.move(last, self._direction)
            last = control

        if default > -1:
            self._value = default
            self._options[default].value = True

        if self._direction == 'B':
            self.height = len(options) * (control.height + _SEPARATION - 2)
        else:
            self.width = len(options) * control.width + _SEPARATION
        return

    def move(self, target, direction='B', X=_SEPARATION_X, Y=_SEPARATION_Y):
        if direction == 'L':
            x = target.x + X + target.width
            y = target.y
        elif direction == 'B':
            x = target.x
            y = target.y + Y + target.height

        self.x = x
        self.y = y

        if self._label:
            self._label.x = x - self._label.width - _SEPARATION_X
            self._label.y = y

        if self._options:
            last = False
            for opt in self._options.values():
                if not last:
                    opt.x = self.x + _SEPARATION_X
                    opt.y = self.y + _SEPARATION
                    last = opt
                    continue
                opt.move(last, self._direction)
                last = opt

        return

    @property
    def value(self):
        return self._value
    @value.setter
    def value(self, value):
        self._value = value


class UnoTab(UnoBaseObject):

    def __init__(self, obj, label):
        super(UnoTab, self).__init__(obj, label)
        self._pages = {k+1: v for k, v in enumerate(self._obj.getControls())}
        self._count = len(self._pages)

    def get_page(self, ID):
        return self._pages[ID]

    def activate(self, ID):
        self._obj.activateTab(ID)
        return

    @property
    def center(self):
        return self._model.Width / 2, self._model.Height / 2

    @property
    def current(self):
        return self._obj.getActiveTabID()

    @catch_exception
    def remove(self, ID=None):
        if ID is None:
            ID = self.current
        page = self.get_page(ID)
        for control in page.getControls():
            page.removeControl(control)
            page.Model.removeByName(control.Model.Name)
        self._obj.removeTab(ID)
        self._model.removeByName('page_{}'.format(ID))
        del self._pages[ID]
        return

    @catch_exception
    def insert(self, title, clone=False):
        if not clone:
            for k, v in self._pages.items():
                if v.Model.Title == title:
                    self.activate(k)
                    return False, 0

        service = 'com.sun.star.awt.UnoPageModel'
        page_model = self._model.createInstance(service)
        page_model.Title = title
        self._count += 1
        ID = self._count
        name = 'page_{}'.format(ID)
        self._model.insertByName(name, page_model)
        self._pages[ID] = self._obj.getControls()[-1]
        self.activate(ID)
        return True, ID


def _get_custom_class(type_control, obj, label=None):
    if not type_control:
        type_control = _get_custom_type(obj.ImplementationName)
    classes = {
        'label': UnoLabel,
        'button': UnoButton,
        'text': UnoText,
        'tab': UnoTab,
        'link': UnoLink,
        'roadmap': UnoRoadmap,
        'listbox': UnoListBox,
        'image': UnoImage,
        'radio': UnoRadio,
        'groupbox': UnoGroupBox,
        'tree': UnoTree,
        'grid': UnoGrid,
    }
    return classes[type_control](obj, label)


def _set_default_properties(default, properties):
    for k, v in default.items():
        if not k in properties:
            properties[k] = v
    return properties


def _validate_properties(type_control, properties):
    if not 'Name' in properties:
        properties['Name'] = type_control
    return


def _default_properties(type_control, properties):
    types = {
        'dialog': {
            'Width': 200,
            'Height': 200,
        },
        'label': {
            'Width': 75,
            'Height': 12,
            'PositionX': 5,
            'PositionY': 5,
            'Tabstop': False,
            'Step': 0,
        },
        'text': {
            'Width': 100,
            'Height': 12,
            'Tabstop': True,
            'PositionX': 5,
            'PositionY': 5,
            'Step': 0,
        },
        'button': {
            'Width': 50,
            'Height': 12,
            'PositionX': 5,
            'PositionY': 5,
            'Tabstop': False,
            'Step': 0,
        },
        'link': {
            'Width': 100,
            'Height': 12,
            'PositionX': 5,
            'PositionY': 5,
            'Step': 0,
        },
        'listbox': {
            'Width': 100,
            'Height': 100,
            'Tabstop': True,
            'PositionX': 5,
            'PositionY': 5,
            'Step': 0,
        },
        'image': {
            'Width': 100,
            'Height': 100,
            'Tabstop': False,
            'PositionX': 5,
            'PositionY': 5,
            'Step': 0,
        },
        'radio': {
            'Width': 50,
            'Height': 12,
            'Tabstop': True,
            'PositionX': 5,
            'PositionY': 5,
            'Step': 0,
        },
        'groupbox': {
            'Width': 100,
            'Height': 25,
            'PositionX': 5,
            'PositionY': 5,
            'Step': 0,
        },
        'tree': {
            'Width': 100,
            'Height': 100,
            'Tabstop': True,
            'SelectionType': 1,
            'PositionX': 5,
            'PositionY': 5,
            'Step': 0,
        },
        'roadmap': {
            'Width': 75,
            'Text': 'Menu',
            'PositionX': 2,
            'PositionY': 2,
            'Step': 0,
        },
        'grid': {
            'Width': 100,
            'Height': 100,
            'PositionX': 5,
            'PositionY': 5,
            'Step': 0,
            'GridDataModel': _create_instance(
                'com.sun.star.awt.grid.DefaultGridDataModel', True),
            'Sizeable': True,
            'ShowColumnHeader': True,
            'ShowRowHeader': True,
            'SelectionModel': 2,
            'UseGridLines': True,
            #~ 'BackgroundColor': COLORS['WHITE'],
        },
    }

    for k, v in types[type_control].items():
        if not k in properties:
            properties[k] = v
    _validate_properties(type_control, properties)
    return


def _add_listeners(obj, events, name=''):
    listeners = {
        'addActionListener': EventsButton(events),
        'addMouseListener': EventsMouse(events),
        'addTabListener': EventsTab(events, name),
        'addFocusListener': EventsFocus(events),
        'addItemListener': EventsRoadmap(events),
        'addSelectionChangeListener': EventsSelectionChange(events),
    }
    link = 'stardiv.Toolkit.UnoFixedHyperlinkControl'
    if obj.ImplementationName == link:
        listeners['addMouseListener'] = EventsMouseLink(events)
    for key, value in listeners.items():
        if hasattr(obj, key):
            getattr(obj, key)(value)
    return


def _add_roadmap_options(model, options):
    for i, v in enumerate(options):
        opt = model.createInstance()
        opt.ID = i
        opt.Label = v
        model.insertByIndex(i, opt)
    return


def _add_grid_columns(columns):
    #~ https://api.libreoffice.org/docs/idl/ref/interfacecom_1_1sun_1_1star_1_1awt_1_1grid_1_1XGridColumn.html
    column_model = _create_instance(
        'com.sun.star.awt.grid.DefaultGridColumnModel', True)
    for column in columns:
        model = _create_instance('com.sun.star.awt.grid.GridColumn', True)
        for k, v in column.items():
            setattr(model, k, v)
        column_model.addColumn(model)
    return column_model


@catch_exception
def _add_control(parent, properties, events):
    model = parent.Model
    tc = properties.pop('Type')
    custom = properties.pop('Custom', {})
    options = properties.pop('Options', None)
    root = properties.pop('Root', '')
    columns = properties.pop('Columns', [])

    if tc == 'grid':
        properties['ColumnModel'] = _add_grid_columns(columns)

    service = _get_instance(tc)
    control_model = model.createInstance(service)

    _default_properties(tc, properties)
    _set_properties(control_model, properties)

    name = properties['Name']
    model.insertByName(name, control_model)

    _add_options(parent, tc, control_model, options)
    control = parent.getControl(name)
    _add_listeners(control, events)

    #~ ToDo
    #~ if tc == 'tree' and root:
        #~ getattr(self, name).root = root

    label = _validate_label(parent, properties, custom)
    control = _get_custom_class(tc, control, label)
    setattr(events, name, control)
    _add_custom(control, custom)
    return name, control


def _add_custom(control, custom):
    if isinstance(control, UnoRadio):
        for k, v in custom.items():
            setattr(control, k, v)
    return


def _add_options(parent, type_control, control_model, options):
    if options is None:
        return

    if type_control == 'roadmap':
        _add_roadmap_options(control_model, options)
        return

    return


def _validate_label(parent, control, custom):
    if not custom:
        return None

    label_value = custom.get('Label', '')
    if not label_value:
        return None

    align = 2
    top = custom.get('Top', False)
    properties = custom.get('LabelProperties', {})
    _default_properties('label', properties)
    name = 'lbl_{}'.format(control['Name'])
    properties['Name'] = name
    properties['Label'] = label_value
    properties['Step'] = control['Step']
    properties['VerticalAlign'] = 1

    if custom.get('Required', False):
        properties['FontHeight'] = 10
        properties['FontWeight'] = 150
        properties['Label'] += ' *'
    if top:
        align = 0
        properties['PositionX'] = control['PositionX']
        properties['PositionY'] = control['PositionY'] - properties['Height'] - _SEPARATION_Y
    else:
        properties['PositionX'] = control['PositionX'] - properties['Width'] - _SEPARATION_X
        properties['PositionY'] = control['PositionY']

    if not 'Align' in properties:
        properties['Align'] = align

    service = _get_instance('label')
    control_model = parent.Model.createInstance(service)
    _set_properties(control_model, properties)
    parent.Model.insertByName(name, control_model)
    label = parent.getControl(name)
    return UnoLabel(label, None)


def _validate_properties_tab(parent, properties):
    if not 'Width' in properties:
        properties['Width'] = parent.width - 10
    if not 'Height' in properties:
        properties['Height'] = parent.height - 10
    if not 'Name' in properties:
        properties['Name'] = 'tab'
    return


def _add_tab(parent, properties, pages):
    name = properties['Name']
    service = 'com.sun.star.awt.UnoMultiPageModel'
    model = parent.Model
    tab_model = model.createInstance(service)
    _set_properties(tab_model, properties)
    model.insertByName(name, tab_model)

    service = 'com.sun.star.awt.UnoPageModel'
    for i, values in enumerate(pages):
        page_model = tab_model.createInstance(service)
        page_model.Title = values['Title']
        tab_model.insertByName('page_{}'.format(i+1), page_model)

    return parent.getControl(name)


class LODialog(object):
    DEFAULT = {
        'Width': 200,
        'Height': 200,
    }

    def __init__(self, properties, name='dlg'):
        self._name = name
        self._modal = properties.pop('Modal', True)
        self._block = properties.pop('Block', False)
        self._controls = []
        self._obj = self._create(properties)
        self._model = self._obj.Model
        self._init_controls()
        self._events = None
        # ~ self._color_on_focus = COLORS['YELLOW']

    @property
    def obj(self):
        return self._obj

    @property
    def model(self):
        return self._model

    @property
    def controls(self):
        return self._controls

    @property
    def visible(self):
        return self._obj.Visible
    @visible.setter
    def visible(self, value):
        self._obj.Visible = value

    @property
    def modal(self):
        return self._modal

    @property
    def block(self):
        return self._block
    @block.setter
    def block(self, value):
        self._block = value

    @property
    def events(self):
        return self._events
    @events.setter
    def events(self, value):
        self._events = value
        self._connect_listeners()

    @property
    def step(self):
        return self._model.Step
    @step.setter
    def step(self, value):
        self._model.Step = value

    def set_properties(self, properties):
        _set_properties(self._model, properties)
        return

    def _create_by_path(self, path):
        dp = _create_instance('com.sun.star.awt.DialogProvider', True)
        dlg_path = _file_url(path)
        return dp.createDialog(dlg_path)

    def _create(self, properties):
        if properties.pop('Custom', False):
            properties.pop('model', None)
            return self._create_custom(properties)

        path = properties.pop('Path', '')
        if path:
            properties.pop('model', None)
            return self._create_by_path(path)

        location = properties.get('Location', 'document')
        library = properties.get('Library', 'Standard')
        name = properties['Name']

        if location == 'share':
            properties.pop('model', None)
            location = _get_path_setting('Basic')[0]
            path = os.path.join(location, library, '{}.xdl'.format(name))
            return self._create_by_path(path)

        model = properties['model']
        dp = SM.createInstanceWithArguments(
            'com.sun.star.awt.DialogProvider', (model,))
        path = 'vnd.sun.star.script:{}.{}?location={}'.format(
            library, name, location)
        return dp.createDialog(path)

    def _create_custom(self, properties):
        dlg = _create_instance('com.sun.star.awt.UnoControlDialog', True)
        model = _create_instance('com.sun.star.awt.UnoControlDialogModel', True)
        toolkit = _create_instance('com.sun.star.awt.Toolkit', True)
        _set_default_properties(self.DEFAULT, properties)
        _set_properties(model, properties)
        dlg.setModel(model)
        dlg.setVisible(False)
        dlg.createPeer(toolkit, None)
        return dlg

    def open(self):
        # ~ self.obj.addTopWindowListener(
            # ~ EventsTopWindow(self.events, self.modal, block))
        if self.modal:
            if self.block:
                result = 0
                while not result:
                    result = self._obj.execute()
                return result
            return self._obj.execute()
        else:
            self.visible = True
        return

    def close(self, value=0):
        if self.modal:
            self._obj.endDialog(value)
        else:
            self.visible = False
        return

    def _get_instance(self, type_control):
        services = {
            'label': 'com.sun.star.awt.UnoControlFixedTextModel',
            'button': 'com.sun.star.awt.UnoControlButtonModel',
            'text': 'com.sun.star.awt.UnoControlEditModel',
            # ~ 'link': 'com.sun.star.awt.UnoControlFixedHyperlinkModel',
            # ~ 'roadmap': 'com.sun.star.awt.UnoControlRoadmapModel',
            # ~ 'listbox': 'com.sun.star.awt.UnoControlListBoxModel',
            # ~ 'image': 'com.sun.star.awt.UnoControlImageControlModel',
            # ~ 'groupbox': 'com.sun.star.awt.UnoControlGroupBoxModel',
            # ~ 'radio': 'com.sun.star.awt.UnoControlRadioButtonModel',
            # ~ 'tree': 'com.sun.star.awt.tree.TreeControlModel',
            # ~ 'grid': 'com.sun.star.awt.grid.UnoControlGridModel',
        }
        return services[type_control.lower()]

    def _get_custom_class(self, type_control, obj, label=None):
        classes = {
            'label': UnoLabel,
            'button': UnoButton,
            'text': UnoText,
            # ~ 'tab': UnoTab,
            # ~ 'link': UnoLink,
            # ~ 'roadmap': UnoRoadmap,
            # ~ 'listbox': UnoListBox,
            # ~ 'image': UnoImage,
            # ~ 'radio': UnoRadio,
            # ~ 'groupbox': UnoGroupBox,
            # ~ 'tree': UnoTree,
            # ~ 'grid': UnoGrid,
        }
        return classes[type_control.lower()](obj, label)

    def _get_default_properties(self, name):
        default = {
            'label': {
                'Width': 75,
                'Height': 12,
                'PositionX': 5,
                'PositionY': 5,
                'Tabstop': False,
            },
            'button': {
                'Width': 50,
                'Height': 12,
                'PositionX': 5,
                'PositionY': 5,
                'Tabstop': False,
            },
            'text': {
                'Width': 100,
                'Height': 12,
                'Tabstop': True,
                'PositionX': 5,
                'PositionY': 5,
            },
        }
        return default[name.lower()]

    def _get_type_control(self, name):
        types = {
            'stardiv.Toolkit.UnoFixedTextControl': 'label',
            'stardiv.Toolkit.UnoButtonControl': 'button',
            'stardiv.Toolkit.UnoEditControl': 'text',
        }
        return types[name]

    def _init_controls(self):
        for control in self.obj.getControls():
            tc = self._get_type_control(control.ImplementationName)
            name = control.Model.Name
            obj = self._get_custom_class(tc, self.obj.getControl(name))
            self._controls.append(obj)
        return

    def _add_listeners(self, control):
        if self.events is None:
            return

        listeners = {
            'addActionListener': EventsButton(self.events),
            'addMouseListener': EventsMouse(self.events),
            'addFocusListener': EventsFocus(self.events),
            # ~ 'addTabListener': EventsTab(self.events, control.name),
            # ~ 'addItemListener': EventsRoadmap(self.events),
            # ~ 'addSelectionChangeListener': EventsSelectionChange(self.events),
        }
        # ~ link = 'stardiv.Toolkit.UnoFixedHyperlinkControl'
        # ~ if obj.ImplementationName == link:
            # ~ listeners['addMouseListener'] = EventsMouseLink(self.events)
        for key, value in listeners.items():
            if hasattr(control.obj, key):
                getattr(control.obj, key)(value)
        setattr(self.events, control.name, control)
        setattr(self, control.name, control)
        return

    def _connect_listeners(self):
        for control in self.controls:
            self._add_listeners(control)
        setattr(self.events, self._name, self)
        return

    def add_control(self, properties):
        tc = properties.pop('Type')
        label = None
        if tc.lower() in ('text',):
            label = properties.pop('Label', None)

        control_model = self.model.createInstance(self._get_instance(tc))
        _set_default_properties(self._get_default_properties(tc), properties)
        _set_properties(control_model, properties)

        name = properties['Name']
        self.model.insertByName(name, control_model)
        control = self._get_custom_class(tc, self.obj.getControl(name), label)
        self._add_listeners(control)

        # ~ label = _validate_label(parent, properties, custom)
        # ~ _add_custom(control, custom)
        # ~ self._validate_control(control)
        return control

    @property
    def color_on_focus(self):
        return self._color_on_focus
    @color_on_focus.setter
    def color_on_focus(self, value):
        self._color_on_focus = value

    @property
    def width(self):
        return self._model.Width

    @property
    def height(self):
        return self._model.Height

    def _validate_control(self, control):
        if isinstance(control, UnoRoadmap):
            if not control.height:
                control.height = self.height - 5
        return

    def _validate_properties_tab(self, properties):
        if not 'Width' in properties:
            properties['Width'] = self.width - _SEPARATION
        if not 'Height' in properties:
            properties['Height'] = self.height - _SEPARATION
        if not 'Name' in properties:
            properties['Name'] = 'tab'
        return

    def add_tab(self, properties, pages):
        self._validate_properties_tab(properties)
        name = properties['Name']
        service = 'com.sun.star.awt.UnoMultiPageModel'
        tab_model = self._model.createInstance(service)
        _set_properties(tab_model, properties)
        self._model.insertByName(name, tab_model)

        service = 'com.sun.star.awt.UnoPageModel'
        for i, values in enumerate(pages):
            page_model = tab_model.createInstance(service)
            page_model.Title = values['Title']
            tab_model.insertByName('page_{}'.format(i), page_model)

        control = self._get_control(name)
        setattr(self, name, self._get_custom_class('tab', control))
        self._add_listeners(control, name)
        return

    def add_control_tab(self, name, index, **kwargs):
        page = getattr(self, name).get_page(index)
        parent = page.getModel()
        tc = kwargs.pop('Type')
        service = _get_instance(tc)
        model = parent.createInstance(service)
        name = kwargs.get('Name')
        self._default_properties(tc, kwargs)
        _set_properties(model, kwargs)
        parent.insertByName(name, model)
        control = page.getControl(name)
        setattr(self, name, self._get_custom_class(tc, control))
        self._add_listeners(control)
        return


class LOBaseForm(object):

    def __init__(self, obj):
        self._obj = obj
        self._con = self._obj.ActiveConnection
        self._db = self._con.Parent.DatabaseDocument
        self._format_date = ''

    @property
    def fields(self):
        return self._obj.Columns.getElementNames()

    @property
    def format_date(self):
        return self._format_date
    @format_date.setter
    def format_date(self, value):
        self._format_date = value

    @property
    def row(self):
        data = {}
        for field in self.fields:
            control = self._obj.Columns[field]
            value = control.Value
            data[field] = value
            if isinstance(value, (Time, Date, DateTime)):
                data[field] = _to_date(value)
                if self._format_date:
                    data[field] = data[field].strftime(self._format_date)
        return data

    @property
    def path(self):
        return _system_path(self._db.getURL())


class LOBase(object):
    DATA_TYPES = {
        'varchar_ignorecase': 'getString',
        #~ MySQL
        'bit': 'getByte',
        'tinyint': 'getLong',
        'int': 'getLong',
        'bigint': 'getLong',
        'bigint unsigned': 'getLong',
        'smallint': 'getLong',
        'integer': 'getLong',
        'integer unsigned': 'getLong',
        'decimal': 'getFloat',
        'float': 'getFloat',
        'double': 'getDouble',
        'char': 'getString',
        'varchar': 'getString',
        'text': 'getString',
        'date': 'getDate',
        'time': 'getTime',
        'timestamp': 'getTimestamp',
        'datetime': 'getTimestamp',
        #~ PostgreSQL
        '': 'getString',
        'int8': 'getLong',
        'int8': 'getLong',
        'unknown': 'getString',
        'bool': 'getBoolean',
        'bytea': 'getBytes',
        'cidr': 'getString',
        'float8': 'getDouble',
        'inet': 'getString',
        'int4': 'getLong',
        'macaddr': 'getString',
        'money': 'getDouble',
        'numeric': 'getDouble',
        'float4': 'getDouble',
        'int2': 'getLong',
        'int4': 'getLong',
        'timestamp without time zone': 'getTimestamp',
        'timestamp with time zone': 'getTimestamp',
        'uuid': 'getString',
        'xml': 'getString',
        #~ SQLite
        'real': 'getDouble',
        'blob': '',
        #~ MSSQL
    }

    def __init__(self, obj=None):
        self._obj = obj
        self._con = None
        self._dbc = _create_instance('com.sun.star.sdb.DatabaseContext')

    @property
    def type(self):
        return 'base'

    @property
    def obj(self):
        return self._obj

    @property
    def title(self):
        return self._obj.DatabaseDocument.getTitle()

    @property
    def con(self):
        return self._con

    def connect(self, name, user='', password=''):
        db = self._dbc[name]
        self._con = db.getConnection(user, password)
        return self._con

    def query(self, sql):
        cursor = self._con.createStatement()
        resulset = cursor.executeQuery(sql)
        return self._parse_data_type(resulset)

    def _parse_data_type(self, resulset):
        if not resulset:
            return ()
        info = resulset.getMetaData()
        cols = range(1, info.getColumnCount() + 1)

        #~ for c in cols:
            #~ dt = info.getColumnTypeName(c).lower()
            #~ print ('DATA TYPE', dt)
            #~ if not DATA_TYPES.get(dt, False):
                #~ log.info(dt)

        cols_type = ('',) + tuple(
            self.DATA_TYPES[info.getColumnTypeName(c).lower()] for c in cols)
        headers = tuple(info.getColumnName(c) for c in cols)
        data = [headers]
        while resulset.next():
            row = tuple(getattr(resulset, cols_type[c])(c) for c in cols)
            data.append(row)
        return tuple(data)


class DocDebug(object):

    def __init__(self, doc):
        self.doc = doc

    def write(self, info):
        self.doc.write(info)
        return


class LOWindow(object):
    CONTROLS = {
        'label': 'FixedText',
    }

    def __init__(self, desktop, properties):
        self._desktop = desktop
        self._frame = None
        self._window = None
        self._container = None
        # ~ self._subcontainer = None
        self._model = None
        self._menu = None
        self._events = None
        self._obj = self._create(properties)

    @property
    def events(self):
        return self._events
    @events.setter
    def events(self, value):
        self._events = value

    @property
    def width(self):
        return self._container.Size.Width

    @property
    def height(self):
        return self._container.Size.Height

    def show(self):
        self._window.addTopWindowListener(
            EventsTopWindow(self.events, window=self._window))
        self._window.addWindowListener(EventsWindow(self.events, self._container))
        self._window.setVisible(True)
        return

    def close(self):
        self._window.dispose()
        self._frame.close(True)
        self._frame = None
        self._window = None
        self._container = None
        # ~ self._subcontainer = None
        self._menu = None
        self._events = None
        return

    def add_menu(self, menus):
        self._create_menu(menus)
        return

    def _create(self, properties):
        ps = (
            properties.get('X', 0),
            properties.get('Y', 0),
            properties.get('Width', 500),
            properties.get('Height', 500),
        )
        title = properties.get('Title', TITLE)
        # ~ menus = properties.get('Menus', {})

        self._create_frame(ps, title)
        self._create_container(ps)
        # ~ self._create_menu(menus)
        # ~ self._create_subcontainer(ps)
        return

    def _create_frame(self, ps, title):
        service = 'com.sun.star.frame.TaskCreator'
        self._frame = (_create_instance(service, True).
            createInstanceWithArguments(
                (NamedValue('FrameName', 'EasyMacro'),
                NamedValue('PosSize', Rectangle(*ps)))))
        self._window = self._frame.getContainerWindow()
        self._frame.setTitle(title)
        self._frame.setCreator(self._desktop)
        self._desktop.getFrames().append(self._frame)
        return

    def _create_container(self, ps):
        toolkit = self._window.getToolkit()
        service = 'com.sun.star.awt.UnoControlContainer'
        self._container = _create_instance(service, True)
        service = 'com.sun.star.awt.UnoControlContainerModel'
        model = _create_instance(service, True)
        model.BackgroundColor = _color('LightGray')
        self._container.setModel(model)
        self._container.createPeer(toolkit, self._window)
        self._container.setPosSize(*ps, POSSIZE)
        self._frame.setComponent(self._container, None)
        return

    def _create_subcontainer(self, ps):
        path = _file_url(_join(CURRENT_PATH, 'dialogs', 'empty.xdl'))
        service = 'com.sun.star.awt.ContainerWindowProvider'
        self._subcontainer = _create_instance(
            service, True).createContainerWindow(
                path, '', self._container.getPeer(), None)
        self._subcontainer.setPosSize(*ps, POSSIZE)
        self._subcontainer.setVisible(True)
        self._container.addControl('subcontainer', self._subcontainer)
        return

    def _create_popupmenu(self, menus):
        menu = _create_instance('com.sun.star.awt.PopupMenu', True)
        for i, v in enumerate(menus):
            cmd = ''
            if isinstance(v, str):
                if v:
                    menu.insertItem(i, v, 0, i)
                    cmd = v.lower().replace(' ', '_')
                else:
                    menu.insertSeparator(i)
            else:
                menu.insertItem(i, v[0], v[2], i)
                cmd = v[1]
                if not cmd:
                    cmd = v[0].lower().replace(' ', '_')
            if cmd:
                menu.setCommand(i, cmd)
                #~ menu.setAcceleratorKeyEvent(i, KeyEvent())
        menu.addMenuListener(EventsMenu(self.events))
        return menu

    def _create_menu(self, menus):
        #~ https://api.libreoffice.org/docs/idl/ref/interfacecom_1_1sun_1_1star_1_1awt_1_1XMenu.html
        #~ nItemId  specifies the ID of the menu item to be inserted.
        #~ aText    specifies the label of the menu item.
        #~ nItemStyle   0 = Standard, CHECKABLE = 1, RADIOCHECK = 2, AUTOCHECK = 4
        #~ nItemPos specifies the position where the menu item will be inserted.
        self._menu = _create_instance('com.sun.star.awt.MenuBar', True)
        for i, v in enumerate(menus):
            self._menu.insertItem(i, v[0], 0, i)
            cmd = v[1]
            if not cmd:
                cmd = v[0].lower().replace(' ', '_')
            self._menu.setCommand(i, cmd)
            #~ self._menu.addMenuListener(EventsMenu(self.events))
            submenu = self._create_popupmenu(v[2])
            self._menu.setPopupMenu(i, submenu)

        self._window.setMenuBar(self._menu)
        return

    def add_tab(self, properties, pages):
        _validate_properties_tab(self, properties)
        control = _add_tab(self._subcontainer, properties, pages)
        setattr(self, properties['Name'], _get_custom_class('tab', control))
        #~ _add_listeners(control, name)
        return

    def add_control_tab(self, tab='tab', index=1, **properties):
        tc = properties['Type']
        page = getattr(self, tab).get_page(index)
        name, control = _add_control(page, properties, self.events)
        setattr(self, name, control)
        return control

    def _create_control(self, properties):
        ct = self.CONTROLS.get(properties.pop('Type'))
        base = 'com.sun.star.awt.UnoControl{}'.format(ct)
        control = _create_instance(base, True)
        model = _create_instance(base + 'Model', True)

        x = properties.pop('PositionX')
        y = properties.pop('PositionY')
        width = properties.pop('Width')
        height = properties.pop('Height')
        names = tuple(properties.keys())
        values = tuple(properties.values())

        model.setPropertyValues(names, values)
        control.setModel(model)
        control.setPosSize(x, y, width, height, POSSIZE)
        return control

    def add_control(self, properties):
        name = properties['Name']
        _default_properties(properties['Type'], properties)
        control = self._create_control(properties)
        self._container.addControl(name, control)
        return


# === LIBO ===

class LIBO(object):
    OS = sys.platform
    NAME_LOG = 'LIBO'
    LANGUAGE = LANGUAGE
    NAME = NAME
    VERSION = VERSION

    def __init__(self):
        self.desktop = _create_instance('com.sun.star.frame.Desktop', True)
        self._start = None

    def color(self, value):
        return _color(value)

    def start(self):
        self._start = datetime.datetime.now()
        return

    @property
    def end(self):
        end = datetime.datetime.now()
        return (end - self._start).total_seconds()

    def call(self, args):
        return subprocess.check_output(args, shell=True).decode()

    def get_info_pc(self):
        """
            https://docs.python.org/3.3/library/platform.html
            Get info PC:
            name user,
            name pc,
            system/OS name,
            machine type,
            Returns the (real) processor name
            string identifying platform with as much useful information as possible,
        """
        info = (
            getpass.getuser(),
            platform.node(),
            platform.system(),
            platform.machine(),
            platform.processor(),
            platform.platform(),
        )
        return info

    @property
    def info_debug(self):
        data = (
            sys.version,
            platform.platform(),
            sys.path,
        )
        paths = '\n'.join(sys.path)
        info = '{}\n\n{}\n\n{}'.format(sys.version, platform.platform(), paths)
        return info

    def create_dialog(self, properties):
        properties['model'] = self.desktop.getCurrentComponent()
        return LODialog(properties)

    def create_window(self, properties={}):
        return LOWindow(self.desktop, properties)

    def get_form(self, event):
        if event.Source.supportsService('com.sun.star.awt.UnoControl'):
            return LOBaseForm(event.Source.Model.Parent)
        return LOBaseForm(event.Source)

    def get_file(self, filters=(), multiple=False):
        dlg_file = _create_instance('com.sun.star.ui.dialogs.FilePicker')
        dlg_file.setTitle('Select file')
        dlg_file.setMultiSelectionMode(multiple)

        if filters:
            dlg_file.setCurrentFilter(filters[0][0])
            for f in filters:
                dlg_file.appendFilter(f[0], f[1])

        if dlg_file.execute():
            if multiple:
                return [_system_path(f) for f in dlg_file.getSelectedFiles()]
            return _system_path(dlg_file.getSelectedFiles()[0])
        return ''

    def get_path_setting(self, name='Work'):
        return _get_path_setting(name)

    def get_path(self, filters=()):
        """
            Options: http://api.libreoffice.org/docs/idl/ref/namespacecom_1_1sun_1_1star_1_1ui_1_1dialogs_1_1TemplateDescription.html
            filters: Example
            (
                ('XML', '*.xml'),
                ('TXT', '*.txt'),
            )
        """
        folder = _create_instance('com.sun.star.ui.dialogs.FilePicker')
        folder.initialize((2,))
        if filters:
            folder.setCurrentFilter(filters[0][0])
            for f in filters:
                folder.appendFilter(f[0], f[1])

        if folder.execute():
            return _system_path(folder.getFiles()[0])
        return ''

    def get_folder(self, init_folder=''):
        folder = _create_instance('com.sun.star.ui.dialogs.FolderPicker')

        if init_folder:
            folder.setDisplayDirectory(_file_url(init_folder))
        else:
            folder.setDisplayDirectory(_file_url(self.get_path_setting()))

        if folder.execute():
            return _system_path(folder.getDirectory())

        return ''

    def get_current_path(self, path=''):
        if not path:
            path = __file__
        return os.path.split(path)[0]

    def open(self, path, opt=None):
        """ Open document in path
            Usually options:
                Hidden: True or False
                AsTemplate: True or False
                ReadOnly: True or False
                Password: super_secret
                MacroExecutionMode: 4 = Activate macros
                Preview: True or False
        """
        path = _file_url(path)
        opt = _properties(opt)
        doc = self.desktop.loadComponentFromURL(path, '_blank', 0, opt)
        return _get_class_doc(doc)

    def open_file(self, path):
        if self.OS == WIN:
            os.startfile(path)
        else:
            subprocess.call(['xdg-open', path])
        return

    def new_doc(self, type_doc='calc'):
        td = 'private:factory/s{}'.format(type_doc)
        doc = self.desktop.loadComponentFromURL(td, '_default', 0, ())
        return _get_class_doc(doc)

    def new_db(self, path):
        dbc = _create_instance('com.sun.star.sdb.DatabaseContext')
        db = dbc.createInstance()
        db.URL = 'sdbc:embedded:hsqldb'
        db.DatabaseDocument.storeAsURL(_file_url(path), ())
        return LOBase(db)

    def get_docs(self):
        if PY2:
            docs = []
            enum = self.desktop.getComponents().createEnumeration()
            while enum.hasMoreElements():
                docs.append(_get_class_doc(enum.nextElement()))
            return docs

        return [_get_class_doc(doc) for doc in self.desktop.getComponents()]

    @property
    def doc(self):
        return self.get_doc()

    def get_doc(self, title=''):
        if title:
            return self._get_doc_by_title(title)
        return _get_class_doc(self.desktop.getCurrentComponent())

    def _get_doc_by_title(self, title):
        if PY2:
            enum = self.desktop.getComponents().createEnumeration()
            while enum.hasMoreElements():
                doc = enum.nextElement()
                if doc.Title == title:
                    return _get_class_doc(doc)

        docs = self.desktop.getComponents()
        for doc in docs:
            if doc.Title == title:
                return _get_class_doc(doc)
        return

    def get_files(self, path, ext='*'):
        docs = []
        for folder, _, files in os.walk(path):
            pattern = re.compile(r'\.{}'.format(ext), re.IGNORECASE)
            docs += [os.path.join(folder, f) for f in files if pattern.search(f)]
        return docs

    def get_path_info(self, path):
        path, filename = os.path.split(path)
        name, extension = os.path.splitext(filename)
        return (path, filename, name, extension)

    def replace_filename(self, path, name):
        path, filename, _, _ = self.get_path_info(path)
        return self.join(path, name)

    def replace_ext(self, path, ext):
        path, _, name, _ = self.get_path_info(path)
        return self.join(path, '{}.{}'.format(name, ext))

    def path_exists(self, path):
        return os.path.exists(path)

    def join(self, *paths):
        return os.path.join(*paths)

    def input(self, message, title=TITLE, default=''):
        value = dialog_input(message, title, default)
        return value

    def msgbox(self, message, title=TITLE, buttons=MSG_BUTTONS.BUTTONS_OK, type_msg='infobox'):
        """ Create message box
            type_msg: infobox, warningbox, errorbox, querybox, messbox
            http://api.libreoffice.org/docs/idl/ref/interfacecom_1_1sun_1_1star_1_1awt_1_1XMessageBoxFactory.html
        """
        toolkit = _create_instance('com.sun.star.awt.Toolkit')
        parent = toolkit.getDesktopWindow()
        mb = toolkit.createMessageBox(parent, type_msg, buttons, title, str(message))
        return mb.execute()

    def question(self, message, title=TITLE):
        res = self.msgbox(message, title, MSG_BUTTONS.BUTTONS_YES_NO, 'querybox')
        return res == YES

    def warning(self, message, title=TITLE):
        return self.msgbox(message, title, type_msg='warningbox')

    def error(self, message, title=TITLE):
        return self.msgbox(message, title, type_msg='errorbox')

    def file_copy(self, source, target='', name=''):
        p, f, n, e = self.get_path_info(source)
        if target:
            p = target
        if name:
            n = name
        path_new = self.join(p, '{}{}'.format(n, e))
        shutil.copy(source, path_new)
        return

    def to_json(self, data, path):
        with open(path, 'w') as f:
            f.write(json.dumps(data, indent=4, sort_keys=True))
        return

    def from_json(self, path):
        with open(path) as f:
            data = json.loads(f.read())
        return data

    def debug(self, data):
        if self.OS == WIN:
            doc = self.get_doc_by_title(FILE_NAME_DEBUG)
            if doc is None:
                return
            out = DocDebug(doc)
            sys.stdout = out
        log.debug(data)
        return

    def log(self, path, data):
        path = _system_path(path)
        with open(path, 'a') as out:
            date = str(datetime.datetime.now())[:19]
            out.write('{} - {} - '.format(date, self.NAME_LOG))
            pprint(data, stream=out)
        return


def dialog_input(message, title, default):

    class ControllersInput(object):

        def cmd_ok_action(self, event):
            self.dlg.close(1)
            return

    properties = {
        'Custom': True,
        'Title': title,
        'Width': 200,
        'Height': 50,
    }
    dlg = LODialog(properties)
    dlg.events = ControllersInput()

    properties = {
        'Type': 'label',
        'Name': 'lbl_msg',
        'Label': message,
        'Width': 140,
        'Height': 25,
        'PositionX': 5,
        'PositionY': 5,
        'MultiLine': True,
    }
    dlg.add_control(properties)

    properties = {
        'Type': 'text',
        'Name': 'txt_value',
        'Text': default,
        'Width': 140,
        'PositionX': 5,
        'PositionY': 30,
    }
    dlg.add_control(properties)

    properties = {
        'Type': 'button',
        'Name': 'cmd_ok',
        'Label': _('OK'),
        'Width': 40,
        'PositionX': 155,
        'PositionY': 10,
        'DefaultButton': True,
        'PushButtonType': 1,
    }
    dlg.add_control(properties)

    properties = {
        'Type': 'button',
        'Name': 'cmd_cancel',
        'Label': _('Cancel'),
        'Width': 40,
        'PositionX': 155,
        'PositionY': 30,
        'PushButtonType': 2,
    }
    dlg.add_control(properties)

    if dlg.open():
        return dlg.txt_value.value

    return ''


