Rapid develop macros in [LibreOffice](https://www.libreoffice.org/)/
[OpenOffice](https://www.openoffice.org/) with [Python](https://www.python.org/).
Look the  [wiki](https://gitlab.com/mauriciobaeza/easy-macro/wikis/home) for more details.

Desarrollo rápido de macros en [LibreOffice](http://www.libreoffice.org/)/
[OpenOffice](https://www.openoffice.org/) con [Python](https://www.python.org/).
Mira el [wiki](https://gitlab.com/mauriciobaeza/easy-macro/wikis/home) para más detalles.


